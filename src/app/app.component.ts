import { Component } from '@angular/core';

import { DigiQuoteMultiContainer } from "@digi/arbetsformedlingen-angular";
import { DigiQuoteSingle } from "@digi/arbetsformedlingen-angular";
import {LayoutBlockVariation, NotificationAlertSize, NotificationAlertVariation} from "@digi/arbetsformedlingen";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'timeseries-frontend-app';
  protected readonly NotificationAlertVariation = NotificationAlertVariation;
  protected readonly NotificationAlertSize = NotificationAlertSize;
  protected readonly LayoutBlockVariation = LayoutBlockVariation;
}
